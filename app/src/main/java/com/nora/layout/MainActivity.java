package com.nora.layout;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.cd_transfer)
    CardView cv_tr;
    @BindView(R.id.cv_ar_vl)
    CardView cv_ar_vl;
    @BindView(R.id.cv_ar_br)
    CardView cv_ar_br;
    @BindView(R.id.cv_ar_cs)
    CardView cv_ar_cs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_releated);

        ButterKnife.bind(this);

        cv_tr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(MainActivity.this);

                dialog.setTitle("Transfer");
                dialog.setContentView(R.layout.transfer_layout);

                Button dialogBtn = (Button) dialog.findViewById(R.id.btn_tr_tr);

                dialogBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        cv_ar_vl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setTitle("Validasi");

                dialog.setContentView(R.layout.account_validation);

                Button dialogButton = (Button)dialog.findViewById(R.id.btn_av_vl);

                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        cv_ar_cs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setTitle("Cek Saldo");

                dialog.setContentView(R.layout.cek_saldo);

                Button dialogButton = (Button)dialog.findViewById(R.id.btn_cs_cs);

                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        cv_ar_br.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setTitle("Pembukaan Rekening");

                dialog.setContentView(R.layout.buka_rekening);

                Button dialogButton = (Button)dialog.findViewById(R.id.btn_br_br);

                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

    }
}
